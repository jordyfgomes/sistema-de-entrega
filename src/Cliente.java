import java.util.List;

public class Cliente extends Pessoa {
	
 	
	Cliente(String nome,String cpf,String endereco,String telefone)
	{
		setNome(nome);
		setCpf(cpf);
		setEndereco(endereco);
		setTelefone(telefone);
	}

	
	
	public void confirmarEntrega(Carga c)
	{
		c.setEntregue(true);
	}
	
	
	
	 
}
 
