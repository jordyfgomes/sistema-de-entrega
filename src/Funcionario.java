public class Funcionario extends Pessoa {
 
	private Double salario;
	 
	private String id;
	 
	private String funcao;
	
	private boolean ativo;

	Funcionario(String nome,String cpf,String endereco,String telefone,Double salario,String id,String funcao,boolean ativo)
	{
		setNome(nome);
		setCpf(cpf);
		setEndereco(endereco);
		setTelefone(telefone);
		this.salario = salario;
		this.id = id;
		this.funcao = funcao;
		this.ativo = ativo;
		
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Double getSalario() {
		return salario;
	}

	public void setSalario(Double salario) {
		this.salario = salario;
	}

	public String getFuncao() {
		return funcao;
	}

	public void setFuncao(String funcao) {
		this.funcao = funcao;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}
	 
}
 
