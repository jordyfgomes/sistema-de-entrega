public class Veículo {
 
	private String tipo;
	 	 
	private String placa;
	 
	private Boolean disponivel;
	
	private int km_rodados;

	
	public Veículo(String string, String string2, boolean bool) {
		this.tipo = string;
		this.placa = string2;
		this.disponivel = bool;
		
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public int getKm_rodados() {
		return km_rodados;
	}

	public void setKm_rodados(int km_rodados) {
		this.km_rodados = km_rodados;
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public Boolean getDisponivel() {
		return disponivel;
	}

	public void setDisponivel(Boolean disponivel) {
		this.disponivel = disponivel;
	}
	 
}
 
