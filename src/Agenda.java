import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class Agenda {
	
	private Date data = new Date();
	private SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy/MM/dd");

	
	private List<Carga> paraentrega = new ArrayList<Carga>();
	
	public Agenda(List<Carga> cargas) {
		
		for(Carga c: cargas)
		{
			if(c.isEntregue() == false && sdf2.format(data).contains(c.getDataEntrega()))
			{
				
				c.setEntregue(true);
				paraentrega.add(c);
			}
		// TODO Auto-generated constructor stub
		}
	}
	
	public void listarEntregas(List<Carga> cargas,List<Cliente> clientes)
	{
		for(Carga c:paraentrega)
			System.out.printf("\n%d| %s \t %.3f KG \t %s \t %s",cargas.indexOf(c),clientes.get(c.getNumerocliente()).getNome(),c.getPeso(),c.getDataChegada(),c.getDataEntrega());
	}
	
	public int getnumeroEntregas()
	{
		return paraentrega.size();
	}
	
	public void paraEntrega(Carga c)
	{
		c.setEntregue(true);
		c.setDataEntrega(data);
		paraentrega.add(c);
	}

}
