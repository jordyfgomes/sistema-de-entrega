import java.text.SimpleDateFormat;
import java.util.Date;

public class Carga {
 
	private Double peso;
	 
	private int numerocliente;
	 
	private Date dataEntrega;
	 
	private Date dataChegada;
	
	private boolean entregue;

	private SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy/MM/dd");

	
	Carga(Double peso, int numerocliente,Date dataEntrega,Date dataChegada,boolean entregue)
	{
		this.peso = peso;
		this.setNumerocliente(numerocliente);
		this.dataEntrega = dataEntrega;
		this.dataChegada = dataChegada;
		this.entregue = entregue;
	}

	public Double getPeso() {
		return peso;
	}

	public void setPeso(Double peso) {
		this.peso = peso;
	}
	

	public String getDataEntrega() {
		return sdf2.format(dataEntrega);
	}

	public void setDataEntrega(Date dataEntrega) {
		this.dataEntrega = dataEntrega;
	}

	public String getDataChegada() {
		SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy/MM/dd HH:mm");
		return sdf1.format(dataChegada);
	}

	public void setDataChegada(Date dataChegada) {
		this.dataChegada = dataChegada;
	}

	public boolean isEntregue() {
		return entregue;
	}

	public void setEntregue(boolean entregue) {
		this.entregue = entregue;
	}

	public int getNumerocliente() {
		return numerocliente;
	}

	public void setNumerocliente(int numerocliente) {
		this.numerocliente = numerocliente;
	}
	 
}
 
