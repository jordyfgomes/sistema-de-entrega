import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;

public interface InterfaceSistema {
 
	public  void cadastrarVeiculo(Veículo v) throws FileNotFoundException, UnsupportedEncodingException;
	public  void fazerEntrega(Carga c);
	public  void cancelarEntrega(Carga c);
	public  void cadastrarFuncionario(Funcionario f) throws FileNotFoundException, UnsupportedEncodingException;
	public  void demitirFuncionario(Funcionario f);
	public  void listarVeiculos();
}
 
