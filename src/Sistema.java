import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

public class Sistema implements InterfaceSistema {
 
	Scanner scanner = new Scanner(System.in);
	static List<Cliente> clientes = new ArrayList<Cliente>();
	static List<Veículo> veiculos = new ArrayList<Veículo>();
	static List<Funcionario> funcionarios = new ArrayList<Funcionario>();
	static List<Carga> cargas = new ArrayList<Carga>();
	static File p = new File("arquivos");
	static File f1 = new File("arquivos/clientes.txt");
	static File f2 = new File("arquivos/veiculos.txt");
	static File f3 = new File("arquivos/funcionarios.txt");
	static File f4 = new File("arquivos/cargas.txt");
	static Agenda agenda;
	private PrintWriter w;


	public static void main(String[] args) throws IOException, InterruptedException {
		Sistema s = new Sistema();
		boolean ativo = true;
		
		while(ativo)
		{
			System.out.println("Há " + agenda.getnumeroEntregas() + " entrega(s) para hoje.\n");
			switch(s.telaInicio())
			{
				case 1:
					s.opcoesCliente();
					break;
				case 2:
					s.opcoesCargas();
					break;
				case 3:
					s.opcoesFuncionarios();
					break;
				case 4:
					s.opcoesVeiculos();
					break;
				case 5:
					ativo = false;
			}
			s.limparTela();
		}
	}

	
	Sistema() throws IOException
	{
		BufferedReader br;
		String linha;
		String[] linhas, linhas2;
		
		if(!(p.exists()))
			p.mkdir();
		if(f1.exists() && f1.length() != 0)
		{
			br = new BufferedReader(new FileReader(f1.getAbsoluteFile()));
			linha = br.readLine();
			linhas = linha.split("#");
			br.close();
			for(int i = 0;i<linhas.length;i++)
			{
				linhas2 = linhas[i].split(",");
				clientes.add(new Cliente(linhas2[0], linhas2[1], linhas2[2], linhas2[3]));
				
			}
		}
		if(f2.exists()  && f2.length() != 0)
		{
			br = new BufferedReader(new FileReader(f2.getAbsoluteFile()));
			linha = br.readLine();
			linhas = linha.split("#");
			br.close();
			for(int i = 0;i<linhas.length;i++)
			{
				linhas2 = linhas[i].split(",");
				veiculos.add(new Veículo(linhas2[0], linhas2[2], Boolean.parseBoolean(linhas2[3])));
				
			}
		}
		if(f3.exists()  && f3.length() != 0)
		{
			br = new BufferedReader(new FileReader(f3.getAbsoluteFile()));
			linha = br.readLine();
			linhas = linha.split("#");
			br.close();
			for(int i = 0;i<linhas.length;i++)
			{
				linhas2 = linhas[i].split(",");
				funcionarios.add(new Funcionario(linhas2[0], linhas2[1], linhas2[2], linhas2[3], Double.parseDouble(linhas2[4]), linhas2[5], linhas2[6],Boolean.parseBoolean(linhas2[7])));
						
				
			}
		}
		if(f4.exists()  && f4.length() != 0)
		{
			br = new BufferedReader(new FileReader(f4.getAbsoluteFile()));
			linha = br.readLine();
			linhas = linha.split("#");
			br.close();
			Date dataChegada = null;
			Date dataEntrega = null;
			for(int i = 0;i<linhas.length;i++)
			{
				linhas2 = linhas[i].split(",");
				try {
					dataEntrega = new SimpleDateFormat("yyyy/MM/dd",Locale.ENGLISH).parse(linhas2[2]);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				try {
					dataChegada = new SimpleDateFormat("yyyy/MM/dd HH:mm",Locale.ENGLISH).parse(linhas2[3]);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				cargas.add(new Carga(Double.parseDouble(linhas2[0]),Integer.parseInt(linhas2[1]),dataEntrega,dataChegada, Boolean.parseBoolean(linhas2[4])));
				
			}
		}
		agenda = new Agenda(cargas);
		
	}
	
	public void opcoesCargas() throws IOException
	{
		System.out.println("1)Cadastrar\n2)Procurar\n3)Realizar Entrega\n4)Listar todas");
		switch(scanner.nextInt())
		{
			case 1:
				while(true)
				{
					 Double peso;
					 
					 Cliente cliente;
					 
					 Date dataEntrega = null;
					 
					 Date date = new Date();
					 SimpleDateFormat dataChegada = new SimpleDateFormat("yyyy/MM/dd HH:mm");
						
					 System.out.println("Digite o número do cliente: ");
					 cliente = clientes.get(scanner.nextInt());
					 if(cliente != null)
					 {
						System.out.printf("\n%d| %s \t: %s \t: %s\n",clientes.indexOf(cliente),cliente.getNome(),cliente.getCpf(),cliente.getCpf());
						System.out.println("\nEste é o cliente correto? [Digite 1 para sim]");
						if(scanner.nextInt() != 1)
							break;
					 }
					 else
					 {
						 System.out.println("Número incorreto!");
						 System.in.read();
						 
					 }
					 System.out.print("Digite o peso da carga: ");
					 peso = scanner.nextDouble();
					 DateFormat data = new SimpleDateFormat("dd/MM/yyyy"); 
					 System.out.println("Digite a data de entrega [dd/mm/aaaa]: ");
					 try
					 {
						scanner.nextLine();
					 	dataEntrega = data.parse(scanner.nextLine());
					 }
					 catch (ParseException ex)
					 {
						 ex.printStackTrace();
					 }
					 try 
					 {
						cargas.add(new Carga(peso, clientes.indexOf(cliente), dataEntrega, dataChegada.parse(dataChegada.format(date)), false));
					 }
					 catch (ParseException e)
					 {
						// TODO Auto-generated catch block
						e.printStackTrace();
					 }
					 break;
				}
				System.out.println("Cadastrado com sucesso!");
				System.in.read();
				gravar();
				break;
			case 2:
				
				break;
			case 3:
				System.out.print("Digite o número da carga(-1 para cancelar): ");
				int numero = scanner.nextInt();
				if(numero >= 0 && cargas.get(numero) != null)
				{
					agenda.paraEntrega(cargas.get(numero));
					System.out.println("Carga adicionada com sucesso!");
					System.in.read();
					gravar();
					return;
				}
				System.out.println("Houve um problema para adicionar a carga");
				break;
			case 4:
				listarCargas();
				System.in.read();
				break;
		}
	}
	
	public void opcoesFuncionarios() throws IOException
	{
		String nome ;
		String	cpf ;
		String telefone;
		String endereco ;
		Double salario;
		String id;
		String funcao;
		limparTela();
		boolean esperando = true;
		while(esperando)
		{
			System.out.println("Funcinario\n1)Cadastrar\n2)Procurar\n3)Listar Todos\n4)Excluir\n5)Voltar");
			switch(scanner.nextInt())
			{
				case 1:
					scanner.nextLine();
					System.out.print("Digite o nome: ");
					nome = scanner.nextLine();
					System.out.print("Digite o cpf(com pontos e traço): ");
					cpf = scanner.next();
					System.out.print("Digite o telefone: ");
					telefone = scanner.next();
					scanner.next();
					System.out.print("Digite o endereco: ");
					endereco = scanner.nextLine();
					System.out.print("Digite o salário (com duas casas decimais): ");
					salario = scanner.nextDouble();
					System.out.print("Digite o cpf(com pontos e traço): ");
					id = scanner.next();
					System.out.print("Digite o telefone: ");
					funcao = scanner.nextLine();
					scanner.next();
					if(nome == null || cpf == null || telefone == null || endereco == null || funcao == null || salario == null || id == null)
						System.out.println("Um dos campos foi deixado em branco, tente de novo.");
					else
					{
						if(funcionarios.add(new Funcionario(nome, cpf, endereco, telefone, salario, id, funcao, true)))
							System.out.println("Funcionario cadastrado com sucesso.");
						else
							System.out.println("Erro ao adicionar Funcionario");
					}
					System.in.read();
				break;
				case 2:
					System.out.println("1)Por Nome\n2)Por cpf\n5)Voltar\n\n");
					switch(scanner.nextInt())
					{
						case 1:
							procurarFuncionarioPorNome();
							break;
						case 2:
							procurarFuncionarioPorId();
							break;
						default: break;
					}
					
					break;
				case 3:
					listarFuncionarios();
					System.in.read();
					break;
				case 4:
					limparTela();
					System.out.println("1)Pelo Número\n2)Pelo Nome\n3)Pelo ID\n4)Cancelar");
					switch(scanner.nextInt())
					{
						case 1:
							limparTela();
							System.out.print("Digite o número: ");
							int numero = scanner.nextInt();
							Cliente c = clientes.get(numero);
							System.out.printf("\n\n%d| %s \t: %s \t: %s\n",clientes.indexOf(c),c.getNome(),c.getCpf(),c.getCpf());
							System.out.println("Deseja apagar o cadastro do cliente [S/N]: ");
							switch(scanner.next().toLowerCase())
							{
								case "s":
									apagarCliente(c);
									break;
								default:
									break;
							}
							break;
						case 2:
							procurarFuncionarioPorNome();
							break;
						case 3:
							procurarFuncionarioPorId();
							break;
						default: break;
						
					}
					break;
				case 5:
					esperando = false;
					break;
			}
			
		limparTela();
		}
	}

	void opcoesVeiculos() throws IOException
	{
		 String tipo = null;
		 		 
		 String placa = null;
		 
		limparTela();
		System.out.println("1)Cadastrar\n2)Atualizar\n3)Remover Cadastro\n4)Voltar");
		switch(scanner.nextInt())
		{
			case 1:
				limparTela();
				scanner.nextLine();
				System.out.print("O tipo é:\n\n1)Caminhão 2)Pickup\n");
				switch(scanner.nextInt())
				{
				 	case 1:
				 		tipo = "Caminhão";
				 		break;
				 	case 2:
				 		tipo = "Pickup";
				 		break;
				 	default:
				 		System.out.println("Digitou o número errado!");
				 		break;
				}
				System.out.print("Digite a placa (Formato: XYZ-1234): ");
				placa = scanner.next();
				
				if(placa == null || tipo == null)
					System.out.println("Um dos campos foi deixado em branco, tente de novo.");
				else
				{
					if(veiculos.add(new Veículo(tipo, placa, true)))
						System.out.println("Cliente cadastrado com sucesso.");
					else
						System.out.println("Erro ao adicionar cliente");
				}
				break;
		}
		gravar();
		System.in.read();
	}
	
	void opcoesCliente() throws InterruptedException, IOException
	{
		
		String nome ;
		String	cpf ;
		String telefone;
		String endereco ;
		limparTela();
		boolean esperando = true;
		while(esperando)
		{
			System.out.println("Cliente\n1)Cadastrar\n2)Procurar\n3)Listar Todos\n4)Excluir\n5)Voltar");
			switch(scanner.nextInt())
			{
				case 1:
					scanner.nextLine();
					System.out.print("Digite o nome: ");
					nome = scanner.nextLine();
					System.out.print("Digite o cpf(com pontos e traço): ");
					cpf = scanner.next();
					System.out.print("Digite o telefone: ");
					telefone = scanner.next();
					scanner.nextLine();
					System.out.print("Digite o endereco: ");
					endereco = scanner.nextLine();
					if(nome == null || cpf == null || telefone == null || endereco == null)
						System.out.println("Um dos campos foi deixado em branco, tente de novo.");
					else
					{
						if(clientes.add(new Cliente(nome, cpf, endereco, telefone)))
						{
							System.out.println("Cliente cadastrado com sucesso.");
							gravar();
						}
						else
							System.out.println("Erro ao adicionar cliente");
					}
					Thread.sleep(1500);
				break;
				case 2:
					System.out.println("1)Por Nome\n2)Por cpf\n5)Voltar\n\n");
					switch(scanner.nextInt())
					{
						case 1:
							procurarClientePorNome();
						break;
						case 2:
							procurarClientePorCpf();
							break;
						default: break;
					}
					break;
				case 3:
					listarClientes();
					System.in.read();
					break;
				case 4:
					limparTela();
					System.out.println("1)Pelo Número\n2)Pelo Nome\n3)Pelo CPF\n4)Cancelar");
					switch(scanner.nextInt())
					{
						case 1:
							limparTela();
							System.out.print("Digite o número: ");
							int numero = scanner.nextInt();
							Cliente c = clientes.get(numero);
							System.out.printf("\n\n%d| %s \t: %s \t: %s\n",clientes.indexOf(c),c.getNome(),c.getCpf(),c.getCpf());
							System.out.println("Deseja apagar o cadastro do cliente [S/N]: ");
							switch(scanner.next().toLowerCase())
							{
								case "s":
									apagarCliente(c);
									break;
								default:
									break;
							}
							break;
						case 2:
							procurarClientePorNome();
							break;
						case 3:
							procurarClientePorCpf();
							break;
						default: break;
						
					}
					break;
				case 5:
					esperando = false;
					break;
			}
			
		limparTela();
		}
	}
	
	public void procurarClientePorNome() throws IOException
	{
		limparTela();
		System.out.print("Digite o nome: ");
		String nome = scanner.next();
		for(Cliente c: clientes)
			if(c.getNome().contains(nome))
				System.out.printf("\n%d| %s \t: %s \t: %s\n",clientes.indexOf(c),c.getNome(),c.getCpf(),c.getCpf());
			System.out.println("\n\nFIM\n");
		System.in.read();
	}
	
	public void procurarFuncionarioPorId()
	{
		limparTela();
		System.out.print("Digite o nome: ");
		String id = scanner.next();
		for(Funcionario c: funcionarios)
			if(c.getId().contains(id))
				System.out.printf("%d| %s \t: %s \t: %s\n",clientes.indexOf(c),c.getNome(),c.getId(),c.getFuncao());
			System.out.println("\n\nFIM\n");
		scanner.next();
	}


	public void procurarFuncionarioPorNome()
	{
		limparTela();
		System.out.print("Digite o nome: ");
		String nome = scanner.next();
		for(Funcionario c: funcionarios)
			if(c.getNome().contains(nome))
				System.out.printf("%d| %s \t: %s \t: %s\n",clientes.indexOf(c),c.getNome(),c.getId(),c.getFuncao());
			System.out.println("\n\nFIM\n");
		scanner.next();
	}


	public void procurarClientePorCpf()
	{
		limparTela();
		System.out.print("Digite o nome: ");
		String nome = scanner.next();
		for(Cliente c: clientes)
			if(c.getNome().contains(nome))
				System.out.printf("\n%d| %s \t: %s \t: %s\n",clientes.indexOf(c),c.getNome(),c.getCpf(),c.getCpf());
			System.out.println("\n\nFIM\n");
		scanner.next();

	}
	
	public void cadastrarCliente(Cliente c) throws FileNotFoundException, UnsupportedEncodingException {
		clientes.add(c);
		gravar();
	}
	 
	public void apagarCliente(Cliente c) throws FileNotFoundException, UnsupportedEncodingException {
		clientes.remove(c);
		gravar();
	}

	
	public void cadastrarVeiculo(Veículo v) throws FileNotFoundException, UnsupportedEncodingException {
		// TODO Auto-generated method stub
		veiculos.add(v);
		gravar();
		
	}

	public void cadastrarCarga(Carga cg) {
		cargas.add(cg);
	}


	public void fazerEntrega(Carga c) {
		// TODO Auto-generated method stub
		c.setEntregue(true);
	}


	public void cancelarEntrega(Carga c) {
		// TODO Auto-generated method stub
		c.setEntregue(false);
	}


	public void cadastrarFuncionario(Funcionario f) throws FileNotFoundException, UnsupportedEncodingException {
		// TODO Auto-generated method stub
		funcionarios.add(f);
		gravar();
	}


	public void demitirFuncionario(Funcionario f) {
		// TODO Auto-generated method stub
		f.setAtivo(false);
	}
	 	
	public void listarFuncionarios()
	{
		for(Funcionario f: funcionarios)
			System.out.printf("\n%d| %s \t: %s \t: %s",funcionarios.indexOf(f),f.getNome(),f.getId(),f.getFuncao());
  	}
	
	public void listarClientes()
	{
		for(Cliente c:clientes)
			System.out.printf("\n%d| %s \t: %s \t: %s",clientes.indexOf(c),c.getNome(),c.getCpf(),c.getCpf());
	}


	@Override
	public void listarVeiculos() {
		// TODO Auto-generated method stub
		for(Veículo v: veiculos)
			System.out.printf("\n%d| %s \t %d \t %s \t %s",veiculos.indexOf(v),v.getTipo(),v.getKm_rodados(),v.getPlaca(),v.getDisponivel());
	}
	
	public void listarCargas()
	{
		for(Carga c: cargas)
			System.out.printf("\n%d| %s \t %.3f KG \t %s \t %s",cargas.indexOf(c),clientes.get(c.getNumerocliente()).getNome(),c.getPeso(),c.getDataChegada(),c.getDataEntrega());

	}	
	
	public int telaInicio()
	{
		System.out.println("1) Cliente\n2) Cargas\n3) Funcionarios\n4) Veículos\n5) Sair");
		return scanner.nextInt();
	}
	
	public void limparTela()
	{
		char c = '\n';
		int length = 25;
		char[] chars = new char[length];
		Arrays.fill(chars, c);
		System.out.print(String.valueOf(chars));
	}
	
	public void gravar() throws FileNotFoundException, UnsupportedEncodingException
	{

		w = new PrintWriter(f1, "UTF-8");
		for(Cliente c: clientes)
			w.printf("%s,%s,%s,%s#",c.getNome(),c.getCpf(),c.getEndereco(),c.getTelefone());
		w.close();
		w = new PrintWriter(f2, "UTF-8");
		for(Veículo v: veiculos)
			w.printf("%s,%d,%s,%s#",v.getTipo(),v.getKm_rodados(),v.getPlaca(),v.getDisponivel());
		w.close();
		w = new PrintWriter(f3, "UTF-8");
		for(Funcionario f: funcionarios)
			w.printf("%s,%s,%s,%s,%f,%s,%s#",f.getNome(),f.getCpf(),f.getEndereco(),f.getTelefone(),f.getSalario(),f.getId(),f.getFuncao());
		w.close();
		w = new PrintWriter(f4, "UTF-8");
		for(Carga c: cargas)
			w.printf("%s,%s,%s,%s,%s#",c.getPeso(),c.getNumerocliente(),c.getDataEntrega(),c.getDataChegada(),c.isEntregue());
		w.close();
	}



}
 
